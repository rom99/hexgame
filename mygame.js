var app = angular.module('app', []);

app.directive('rmQuickStatus', function() {
	return {
		restrict: 'E',
		template: '<span class="status-item">\
				<label><i class="fa fa-shopping-bag" style="color: tan;"></i></label><span> {{person.getBurden()/person.strength * 100 | number: 0}}%</span>\
			</span>\
			<span class="status-item">\
				<label><i class="fa fa-dollar" style="font-size: larger; color: steelblue;"></i></label><span>{{person.cash | currency : \'\' : 0}}</span>\
			</span>\
			<span class="status-item">\
				<label><i class="fa fa-heart" style="color: red;"></i></label><span>{{person.health}}</span>\
			</span>\
			<span class="status-item">\
			<label>\
				<span class="fa-stack">\
					<i class="fa fa-circle fa-stack-2x" style="color: #eea;"></i><i class="fa fa-stack-2x" style="color: black;" ng-class="{\'fa-smile-o\': person.morale >= 60, \'fa-meh-o\': person.morale > 40 && person.morale < 60, \'fa-frown-o\': person.morale <= 40, \'fa-rotate-90\': person.health < 0}"></i>\
				</span></label><span>{{person.morale | number: 0}}</span>\
			</span>',
		scope: {
			person: '='
		}
	};
})

app.service('GameModels', function () {
	var m = {};

	m.player = {
      name: 'unknown',
      sex: 'unknown',
      health: 100,
      hunger: 5,
      morale: 100,
      cash: 0,
      location: 'unknown',
      strength: 10,
      possessions: [],
      getBurden: function() {
    		var totalBurden = 0;
    		
    		(function getBurden(contents) {
    			_.each(contents, function(item) {
    				totalBurden += item.burden;
    				if(item.contents) {
    					getBurden(item.contents);
    				}
    			});
    		})(this.possessions);
    		return totalBurden;
    	}
  };

  m.person = angular.extend({}, m.player, {
      companions: [],
      cash: 3000
  });

  m.item = {
      type: 'unknown',
      name: 'unknown',
      burden: 1
  };

  m.bag = angular.extend({}, m.item, {
      type: 'bag',
      capacity: 1
  });

  m.backpack = angular.extend({}, m.bag, {
  	name: 'backpack',
  	capacity: 6
  });

  m.clothing = angular.extend({}, m.item, {
      type: 'clothing',
      warmth: 1,
      waterproofing: 1
  });

  m.waterproofJacket = angular.extend({}, m.clothing, {
		name: 'waterproof jacket',
		warmth: 3,
		waterproofing: 7
	});

  m.food = angular.extend({}, m.item, {
      type: 'food',
      nourishment: 1
  });

  m.rice = angular.extend({}, m.food, {
  	nourishment: 6
  });

  m.tool = angular.extend({}, m.item, {
      type: 'tool',
      strength: 1,
      usedFor: []
  });

  m.machete = angular.extend({}, m.tool, {
  	name: 'machete',
  	strength: 5,
  	usedFor: ['clearing', 'cutting', 'attack', 'defence']
  });

  m.weapon = angular.extend({}, m.item, {
      type: 'weapon',
      strength: 1,
      accuracy: 1
  });

  m.shotgun = angular.extend({}, m.weapon, {
  	name: 'shotgun',
  	strength: 9,
  	accuracy: 5,
    usedFor: ['attack']
  });

  m.medicine = angular.extend({}, m.item, {
      type: 'medicine',
      usedFor: []
  }),

  m.antibiotic = angular.extend({}, m.medicine, {
  	name: 'antibiotic',
  	usedFor: ['infection']
  });

  m.inventoryItem = {
      item: {},
      unit: 'unknown',
      volume: 0,
      price: 0
  };

  m.location = {
      name: 'unknown',
      weather: 'unknown',
      support: 50,
      amenities: []
  };

  m.amenity = {
      type: 'unknown',
      name: 'unknown',
      stock: []
  };

  m.farm = angular.extend({}, m.amenity, {
      type: 'farm'
  });

  m.shop = angular.extend({}, m.amenity, {
      type: 'shop'
  });

	console.log('models', m);

	/** Create a new copy of any available GamemModel type and initialise using options (optional). */
	this.create = function(type, options) {
		var prototype = m[type];
		if(!_.isUndefined(prototype)) {
			return _.isEmpty(options) ? 
				angular.copy(prototype) : 
				angular.extend({}, prototype, options);
		}
	};
});

app.controller('gameCtrl', ['$scope', 'GameModels', 'keyCodeMap', function($scope, models, keyCodes) {
	console.log('gamectrl loaded');

	var weathers = ['raining', 'sunny', 'humid'];

	var locations = {
		perRow: 20,
		numRows: 20,
		list: []
	};

	locations.generate = function() {
		for(var i = 0; i < locations.numRows; i++) {

			var odd = i % 2 === 1;
			var perRow = locations.perRow;
			if(odd) {
				perRow -= 1;
			}

			var row = [];

			for(var j = 0; j < perRow; j++) {
				row.push(
					models.create('location', {
						name: 'generated-' + (i + j),
						weather: weathers[j % 3],
						support: Math.random() * 100,
						amenities: [],
						index: [i,j],
						odd: odd
					})
				);
			}

			locations.list.push(row);
		}
	};
	locations.generate();

	var player = models.create('player', {
		name: 'Ross',
		sex: 'male',
		location: locations.list[0][0],
		cash: 3000,
		companions: [
			models.create('person', {
				name: 'Frederico',
				sex: 'male'
			}),
			models.create('person', {
				name: 'Gertrude',
				sex: 'female'
			})
		],
		possessions: [
			models.create('backpack', {
				contents: [
					models.create('waterproofJacket'),
					models.create('rice')
				]
			}),
			models.create('machete')
		]
	});

  var enemy = models.create('person', {
    name: 'Archie',
    sex: 'male',
    location: _.last(_.last(locations.list)),
    possessions: [
      models.create('machete')
    ]
  });

	var people = _.flatten([player, player.companions, enemy]);
	var weatherEffect = {
		raining: { morale: -2 },
		humid: { morale: -1 },
		sunny: { morale: 1 }
	};

  function gameOver(reason) {
    $scope.status = 'GAME OVER! ' + reason;
  };

	function applyHunger(person) {
		var tolerance = 10;
		person.hunger++;
		if(person.hunger > tolerance) {
			person.health -= person.hunger - tolerance;
			person.morale -= person.hunger - tolerance;
		}
	}

	function applyLocation(person) {
		var supportTolerance = 50;
		var loc = person.location;
		if(loc) {
			if(loc.weather) {
				person.morale += weatherEffect[loc.weather].morale;
			}
			if(loc.support < supportTolerance) {
				person.morale -= supportTolerance - loc.support;
			}
		}
	}

	function applyBurden(person) {
		var totalBurden = person.getBurden();
		if(totalBurden > person.strength) {
			person.morale -= totalBurden - person.strength;
		}
		if(totalBurden > 1.2 * person.strength) {
			person.health -= totalBurden - (1.2 * person.strength);
		}
	}

	$scope.getOpacity = function(location) {
		return Math.random();
	}

	function nextTurn() {
		_.each(people, function(person) {
			applyHunger(person);
			applyLocation(person);
			applyBurden(person);
		});

    calcEnemyMove();

		if(player.health <= 0) {
			gameOver('You died.');
		}
		else if(player.morale <= 0) {
			gameOver('You gave up.');
		}
	};

	$scope.getLocation = function(location) {
		return _.find(locations, { name: location });
	};

  function calcEnemyMove() {
    var pp = player.location.index,
        ep = enemy.location.index,
        em = [0,0];

    _.each(em, function(el, i) {
      var d = pp[i] - ep[i];
      if(d < 0) em[i] = -1;
      if(d > 0) em[i] = 1;
    });

    moveEnemy(em);
  }

  var keyEvents = {
      W: [-1, -1],
      E: [-1, 0],
      A: [0, -1],
      D: [0, 1],
      Z: [1, -1],
      X: [1, 0]
  };

  function move(person, m) {
    var lx = person.location.index,
      odd = person.location.odd,
      l = locations.list,
      row = lx[0] + m[0],
      pos = lx[1] + m[1],
      newLoc;
    
    if(odd && m[0] !== 0) {
      pos++;
    } 

    newLoc = l[row][pos];

    if(typeof newLoc !== 'undefined') {
      person.location = newLoc;
      return true;
    }
    return false;
  }

  var movePlayer = _.partial(move, player);
  var moveEnemy = _.partial(move, enemy);

	$scope.handleKey = function(e) {
    var key = keyCodes[e.keyCode],
        moveEvent = keyEvents[key],
        moved = false;

    console.log(key, moveEvent);

    if(typeof moveEvent !== 'undefined') {
      moved = movePlayer(moveEvent);
    }
    if(moved) {
      nextTurn();
    }
	};

	$scope.player = player;
	$scope.locations = locations;
  $scope.enemy = enemy;
	$scope.status = 'Playing...';

  console.log('enemy', $scope.enemy);
}]);

app.value('keyCodeMap', 
// See answer by Dave Alger https://stackoverflow.com/questions/1772179/get-character-value-from-keycode-in-javascript-then-trim/23377822#23377822
// names of known key codes (0-255)
[
  "", // [0]
  "", // [1]
  "", // [2]
  "CANCEL", // [3]
  "", // [4]
  "", // [5]
  "HELP", // [6]
  "", // [7]
  "BACK_SPACE", // [8]
  "TAB", // [9]
  "", // [10]
  "", // [11]
  "CLEAR", // [12]
  "ENTER", // [13]
  "ENTER_SPECIAL", // [14]
  "", // [15]
  "SHIFT", // [16]
  "CONTROL", // [17]
  "ALT", // [18]
  "PAUSE", // [19]
  "CAPS_LOCK", // [20]
  "KANA", // [21]
  "EISU", // [22]
  "JUNJA", // [23]
  "FINAL", // [24]
  "HANJA", // [25]
  "", // [26]
  "ESCAPE", // [27]
  "CONVERT", // [28]
  "NONCONVERT", // [29]
  "ACCEPT", // [30]
  "MODECHANGE", // [31]
  "SPACE", // [32]
  "PAGE_UP", // [33]
  "PAGE_DOWN", // [34]
  "END", // [35]
  "HOME", // [36]
  "LEFT", // [37]
  "UP", // [38]
  "RIGHT", // [39]
  "DOWN", // [40]
  "SELECT", // [41]
  "PRINT", // [42]
  "EXECUTE", // [43]
  "PRINTSCREEN", // [44]
  "INSERT", // [45]
  "DELETE", // [46]
  "", // [47]
  "0", // [48]
  "1", // [49]
  "2", // [50]
  "3", // [51]
  "4", // [52]
  "5", // [53]
  "6", // [54]
  "7", // [55]
  "8", // [56]
  "9", // [57]
  "COLON", // [58]
  "SEMICOLON", // [59]
  "LESS_THAN", // [60]
  "EQUALS", // [61]
  "GREATER_THAN", // [62]
  "QUESTION_MARK", // [63]
  "AT", // [64]
  "A", // [65]
  "B", // [66]
  "C", // [67]
  "D", // [68]
  "E", // [69]
  "F", // [70]
  "G", // [71]
  "H", // [72]
  "I", // [73]
  "J", // [74]
  "K", // [75]
  "L", // [76]
  "M", // [77]
  "N", // [78]
  "O", // [79]
  "P", // [80]
  "Q", // [81]
  "R", // [82]
  "S", // [83]
  "T", // [84]
  "U", // [85]
  "V", // [86]
  "W", // [87]
  "X", // [88]
  "Y", // [89]
  "Z", // [90]
  "OS_KEY", // [91] Windows Key (Windows) or Command Key (Mac)
  "", // [92]
  "CONTEXT_MENU", // [93]
  "", // [94]
  "SLEEP", // [95]
  "NUMPAD0", // [96]
  "NUMPAD1", // [97]
  "NUMPAD2", // [98]
  "NUMPAD3", // [99]
  "NUMPAD4", // [100]
  "NUMPAD5", // [101]
  "NUMPAD6", // [102]
  "NUMPAD7", // [103]
  "NUMPAD8", // [104]
  "NUMPAD9", // [105]
  "MULTIPLY", // [106]
  "ADD", // [107]
  "SEPARATOR", // [108]
  "SUBTRACT", // [109]
  "DECIMAL", // [110]
  "DIVIDE", // [111]
  "F1", // [112]
  "F2", // [113]
  "F3", // [114]
  "F4", // [115]
  "F5", // [116]
  "F6", // [117]
  "F7", // [118]
  "F8", // [119]
  "F9", // [120]
  "F10", // [121]
  "F11", // [122]
  "F12", // [123]
  "F13", // [124]
  "F14", // [125]
  "F15", // [126]
  "F16", // [127]
  "F17", // [128]
  "F18", // [129]
  "F19", // [130]
  "F20", // [131]
  "F21", // [132]
  "F22", // [133]
  "F23", // [134]
  "F24", // [135]
  "", // [136]
  "", // [137]
  "", // [138]
  "", // [139]
  "", // [140]
  "", // [141]
  "", // [142]
  "", // [143]
  "NUM_LOCK", // [144]
  "SCROLL_LOCK", // [145]
  "WIN_OEM_FJ_JISHO", // [146]
  "WIN_OEM_FJ_MASSHOU", // [147]
  "WIN_OEM_FJ_TOUROKU", // [148]
  "WIN_OEM_FJ_LOYA", // [149]
  "WIN_OEM_FJ_ROYA", // [150]
  "", // [151]
  "", // [152]
  "", // [153]
  "", // [154]
  "", // [155]
  "", // [156]
  "", // [157]
  "", // [158]
  "", // [159]
  "CIRCUMFLEX", // [160]
  "EXCLAMATION", // [161]
  "DOUBLE_QUOTE", // [162]
  "HASH", // [163]
  "DOLLAR", // [164]
  "PERCENT", // [165]
  "AMPERSAND", // [166]
  "UNDERSCORE", // [167]
  "OPEN_PAREN", // [168]
  "CLOSE_PAREN", // [169]
  "ASTERISK", // [170]
  "PLUS", // [171]
  "PIPE", // [172]
  "HYPHEN_MINUS", // [173]
  "OPEN_CURLY_BRACKET", // [174]
  "CLOSE_CURLY_BRACKET", // [175]
  "TILDE", // [176]
  "", // [177]
  "", // [178]
  "", // [179]
  "", // [180]
  "VOLUME_MUTE", // [181]
  "VOLUME_DOWN", // [182]
  "VOLUME_UP", // [183]
  "", // [184]
  "", // [185]
  "SEMICOLON", // [186]
  "EQUALS", // [187]
  "COMMA", // [188]
  "MINUS", // [189]
  "PERIOD", // [190]
  "SLASH", // [191]
  "BACK_QUOTE", // [192]
  "", // [193]
  "", // [194]
  "", // [195]
  "", // [196]
  "", // [197]
  "", // [198]
  "", // [199]
  "", // [200]
  "", // [201]
  "", // [202]
  "", // [203]
  "", // [204]
  "", // [205]
  "", // [206]
  "", // [207]
  "", // [208]
  "", // [209]
  "", // [210]
  "", // [211]
  "", // [212]
  "", // [213]
  "", // [214]
  "", // [215]
  "", // [216]
  "", // [217]
  "", // [218]
  "OPEN_BRACKET", // [219]
  "BACK_SLASH", // [220]
  "CLOSE_BRACKET", // [221]
  "QUOTE", // [222]
  "", // [223]
  "META", // [224]
  "ALTGR", // [225]
  "", // [226]
  "WIN_ICO_HELP", // [227]
  "WIN_ICO_00", // [228]
  "", // [229]
  "WIN_ICO_CLEAR", // [230]
  "", // [231]
  "", // [232]
  "WIN_OEM_RESET", // [233]
  "WIN_OEM_JUMP", // [234]
  "WIN_OEM_PA1", // [235]
  "WIN_OEM_PA2", // [236]
  "WIN_OEM_PA3", // [237]
  "WIN_OEM_WSCTRL", // [238]
  "WIN_OEM_CUSEL", // [239]
  "WIN_OEM_ATTN", // [240]
  "WIN_OEM_FINISH", // [241]
  "WIN_OEM_COPY", // [242]
  "WIN_OEM_AUTO", // [243]
  "WIN_OEM_ENLW", // [244]
  "WIN_OEM_BACKTAB", // [245]
  "ATTN", // [246]
  "CRSEL", // [247]
  "EXSEL", // [248]
  "EREOF", // [249]
  "PLAY", // [250]
  "ZOOM", // [251]
  "", // [252]
  "PA1", // [253]
  "WIN_OEM_CLEAR", // [254]
  "" // [255]
]);
